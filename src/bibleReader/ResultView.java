package bibleReader;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Arrays;

import javax.swing.JButton;
import javax.swing.JEditorPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;

import bibleReader.model.Bible;
import bibleReader.model.BibleReaderModel;
import bibleReader.model.NavigableResults;
import bibleReader.model.Reference;
import bibleReader.model.ResultType;
import bibleReader.model.Verse;
import bibleReader.model.VerseList;

/**
 * The display panel for the Bible Reader.
 * 
 * @author cusack
 * @author Daniel Keith
 * @author Evan Mulshine
 * @author Dre Solorzano
 */
public class ResultView extends JPanel {

	private BibleReaderModel model;
	private NavigableResults results;
	private JScrollPane scrollPane;
	public JEditorPane editorPane;
	private JTextField textField;
	private JTextField pageField;
	public JButton nextButton;
	public JButton previousButton;
	private JPanel bottomPanel;
	private JPanel textPanel;
	private JPanel pagePanel;
	

	/**
	 * Construct a new ResultView and set its model to myModel. It needs a model to
	 * look things up.
	 * 
	 * @param myModel The model this view will access to get information.
	 */
	public ResultView(BibleReaderModel myModel) {
		model = myModel;
		results = new NavigableResults(new ArrayList<Reference>(), null, ResultType.NONE);
		setUpGUI();
		updateButtons();
	}

	/**
	 * Set up the GUI.
	 */
	public void setUpGUI() {
		// Set up the results pane.
		editorPane = new JEditorPane();
		editorPane.setName("OutputEditorPane");
		editorPane.setContentType("text/html");
		editorPane.setEditable(false);
		scrollPane = new JScrollPane(editorPane, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
				JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		
		// Set up the summary stats field.
		textField = new JTextField(40);
		textField.setEditable(false);
		
		// Set up the page number field.
		pageField = new JTextField(15);
		pageField.setEditable(false);
		
		// Set up the page change buttons.
		nextButton = new JButton("Next");
		nextButton.setName("NextButton");
		nextButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				displayResults(results.nextResults());
				pageField.setText("Displaying page " + results.getPageNumber() + " of " + results.getNumberPages() + ".");
			}
		});
		
		previousButton = new JButton("Previous");
		previousButton.setName("PreviousButton");
		previousButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				displayResults(results.previousResults());
				pageField.setText("Displaying page " + results.getPageNumber() + " of " + results.getNumberPages() + ".");
			}
		});
		
		// Set up the bottom panel.
		bottomPanel = new JPanel();
		bottomPanel.setLayout(new BorderLayout());
		textPanel = new JPanel();
		textPanel.add(textField);
		pagePanel = new JPanel();
		pagePanel.add(pageField);
		pagePanel.add(previousButton);
		pagePanel.add(nextButton);
		bottomPanel.add(textPanel, BorderLayout.WEST);
		bottomPanel.add(pagePanel, BorderLayout.EAST);
		
		// Add everything to the ResultView panel.
		setLayout(new BorderLayout());
		add(scrollPane, BorderLayout.CENTER);
		add(bottomPanel, BorderLayout.SOUTH);

	}

	/**
	 * Search for the given word and display the results.
	 * 
	 * @param input The word to be searched.
	 */
	public void search(String input) {
		// Get the search results to pass into displayResults.
		ArrayList<Reference> refs = model.getReferencesContainingAllWordsAndPhrases(input);
		results = new NavigableResults(refs, input, ResultType.SEARCH);
		
		displayResults(results.currentResults());
		
		// Set the summary stats and page text.
		if (refs.size() == 0) {
			textField.setText("There were no results for \"" + input + "\".");
		} else {
			textField.setText("There are " + refs.size() + " verses containing the phrase \"" + input + "\".");
			pageField.setText("Displaying page " + results.getPageNumber() + " of " + results.getNumberPages() + ".");
		}

	}
	
	/**
	 * Display the given passage. 
	 * 
	 * @param input The passage to be displayed.
	 */
	public void passage(String input) {
		// Get the search results to pass into displayResults.
		ArrayList<Reference> refs = model.getReferencesForPassage(input);
		results = new NavigableResults(refs, input, ResultType.PASSAGE);
		
		displayResults(results.currentResults());		
		// Set the summary stats and page text.
		if (refs.size() == 0) {
			textField.setText("There were no results for \"" + input + "\".  Please enter a valid passage (e.g. John 3:16-18).");
		} else {
			textField.setText("There are " + refs.size() + " results for the passage \"" + input + "\".");
			pageField.setText("Displaying page " + results.getPageNumber() + " of " + results.getNumberPages() + ".");
		}
	}
	
	/**
	 * Display all the verses from the given references
	 * 
	 * @param refs The given references
	 */
	public void displayResults(ArrayList<Reference> refs) {
		// Check if there were no results.
		if (refs.size() == 0) {
			editorPane.setText("No results");
		} else {
			String[] versions = model.getVersions();
			StringBuilder stringBuilder = new StringBuilder();
			
			// Display the results for a word search.
			if (results.getType() == ResultType.SEARCH) {
				stringBuilder.append("<table style=\"width:100%\">");
				stringBuilder.append("<tr valign=\"top\"><th></th>");
				
				// Loop through the versions to make a header for each.
				for (int i = 0 ; i < versions.length ; i++) {
					stringBuilder.append("<th>" + versions[i] + "</th>");
				}
				stringBuilder.append("</tr>");
				
				// Loop through to display the references.
				for (Reference ref : refs) {
					stringBuilder.append("<tr valign=\"top\"><td><b>" + ref + "</b></td>");
					// Loop through to display the verse from each version.
					for (int i = 0 ; i < versions.length ; i++) {
						stringBuilder.append("<td>" + model.getText(versions[i], ref) + "</td>");
					}
				}
				
				// Make each occurrence of the search term bold.
				ArrayList<String> words = extractWords(results.getQueryPhrase());
				String builderText = stringBuilder.toString();
				for (String word : words) {
					builderText = builderText.replaceAll("(?i)(?<!\\w)" + word + "(?!\\w)", "<b>$0</b>");
				}
				stringBuilder = new StringBuilder(builderText);
			}
			else {
				
				// Display the passage at the top of the results.
				Reference firstRef = refs.get(0);
				Reference lastRef = refs.get(refs.size()-1);
				
				if (firstRef.getChapter() == lastRef.getChapter()) {
					// Display the passage in form "book chapter:verse1-verse2".
					stringBuilder.append("<center><b>" + firstRef.getBook() + " " + firstRef.getChapter() + ":" + 
							firstRef.getVerse() + "-" + lastRef.getVerse());
				} else {
					// Display the passage in form "book chapter1:verse1-chapter2:verse2".
					stringBuilder.append("<center><b>" + firstRef.getBook() + " " + firstRef.getChapter() + ":" + 
							firstRef.getVerse() + "-" + lastRef.getChapter() + ":" + lastRef.getVerse());
				}
				stringBuilder.append("<table style=\"width:100%\">");
				stringBuilder.append("<tr valign=\"top\">");
				
				// Loop through the versions to display a header for each.
				for (int i = 0 ; i < versions.length ; i++) {
					stringBuilder.append("<th>" + versions[i] + "</th>");
				}
				stringBuilder.append("</tr><tr valign=\\\"top\\\">");
				
				for (int i = 0 ; i <versions.length ; i++) {
					stringBuilder.append("<td>");
					int index =0;
					
					for (Reference ref : refs) {
						int chapter = ref.getChapter();
						int verse = ref.getVerse();
						String text = model.getText(versions[i], ref);
						if (text.equals("")) {
							// The reference isn't actually included in this version, so do nothing.
						}
						else if (index == 0 && verse == 1) {
							// This is the first reference in the results and the first verse in a chapter, so format it a certain way.
							stringBuilder.append("<p><sup>" + chapter + "</sup>" + model.getText(versions[i], ref));
						}
						else if (index != 0 && verse ==1) {
							// This is the first verse in a chapter but not the first reference in the results, so put extra space before it.
							stringBuilder.append("</p><p></p><p><sup>" + chapter + "</sup>" + model.getText(versions[i], ref)); 
						}
						else {
							// This is a verse other than the first in the chapter, so format it a certain way.
							stringBuilder.append("<sup>" + verse + "</sup>" + text);
						}
						index++;
					}
					stringBuilder.append("</p></td>");
				}
				stringBuilder.append("</tr>");
			}
			stringBuilder.append("</table>");
			editorPane.setText(stringBuilder.toString());
		}
		editorPane.setCaretPosition(0);
		updateButtons();
	}
	
	/**
	 * Check to see if the page change buttons need to be disabled and change them as necessary.
	 */
	public void updateButtons() {
		if(!results.hasNextResults()) {
			nextButton.setEnabled(false);
		} else {
			nextButton.setEnabled(true);
		}
		if(!results.hasPreviousResults()) {
			previousButton.setEnabled(false);
		} else {
			previousButton.setEnabled(true);
		}
	}
	
	/**
	 * Refresh the results.
	 */
	public void refresh() {
		String lastInput = results.getQueryPhrase();
		if (results.getType() == ResultType.SEARCH) {
			search(lastInput);
		}
		else if (results.getType() == ResultType.PASSAGE){
			passage(lastInput);
		}
	}
	
	public static ArrayList<String> extractWords (String text) {
		text = text.toLowerCase();
		// removes a few HTML tags (relevant to ESV) and 's at end of words.
		// Replaces them with space so words around them don't get squashed
		// together. Notice the two types of apostrophe-each is used in a 
		// different version.
		text = text.replaceAll("(<sup>[,\\w]*?</sup>|'s|�s|&#\\w*;|,)", " ");
		//remove commas. This should help us match numbers better.
		text = text.replaceAll(",",  "");
		String [] words = text.split("\\W+");
		ArrayList<String> toRet = new ArrayList<String>(Arrays.asList(words));
		toRet.remove("");
		return toRet;
	}
}
