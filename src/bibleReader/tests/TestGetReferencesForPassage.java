package bibleReader.tests;

import static org.junit.Assert.*;

import java.io.File;
import java.util.ArrayList;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.Timeout;

import bibleReader.BibleIO;
import bibleReader.model.ArrayListBible;
import bibleReader.model.Bible;
import bibleReader.model.BibleReaderModel;
import bibleReader.model.Reference;
import bibleReader.model.Verse;
import bibleReader.model.VerseList;

/**
 * Tests for the getReferencesForPassage method of the BibleReaderModel class. 
 * These tests assume BibleIO is working an can read in the kjv.atv file.
 * 
 * @author Evan Mulshine
 * @author Merrik Campagna
 * @author Dre Solorzano
 */
public class TestGetReferencesForPassage {
	@Rule
	public Timeout globalTimeout = new Timeout(1000);
	
	private static VerseList	versesFromFile;
	private BibleReaderModel	model;

	@BeforeClass
	public static void readFile() {
		// Our tests will be based on the KJV version for now.
		File file = new File("kjv.atv");
		// We read the file here so it isn't done before every test.
		versesFromFile = BibleIO.readBible(file);
	}

	@Before
	public void setUp() throws Exception {
		// Make a shallow copy of the verses.
		ArrayList<Verse> copyOfList =versesFromFile.copyVerses();
		// Now make a copy of the VerseList
		VerseList copyOfVerseList = new VerseList(versesFromFile.getVersion(), versesFromFile.getDescription(),
				copyOfList);

		Bible testBible = new ArrayListBible(copyOfVerseList);
		model = new BibleReaderModel();
		model.addBible(testBible);
	}

	public VerseList getVersesForReference(String reference) {
		ArrayList<Reference> list = model.getReferencesForPassage(reference);
		VerseList results = model.getVerses("KJV", list);
		return results;
	}
	
	@Test
	public void testSingleVerseValid() {
		VerseList johnResults = getVersesForReference("John 3 : 16");
		VerseList genResults = getVersesForReference("Gen 1:1");
		VerseList revResults = getVersesForReference("Revelation 22:21");
		
		assertArrayEquals(versesFromFile.subList(26136, 26137).toArray(), johnResults.toArray());
		assertArrayEquals(versesFromFile.subList(0, 1).toArray(), genResults.toArray());
		assertArrayEquals(versesFromFile.subList(31101, 31102).toArray(), revResults.toArray());
	}
	
	@Test
	public void testFromSingleChapterValid() {
		VerseList eccResults = getVersesForReference(" Ecclesiastes 3 : 1 - 8 ");
		VerseList joshResults = getVersesForReference("Joshua 24:28-33");
		VerseList psalmResults = getVersesForReference("Psalm 23:1-6");
		
		assertArrayEquals(versesFromFile.subList(17360, 17368).toArray(), eccResults.toArray());
		assertArrayEquals(versesFromFile.subList(6504, 6510).toArray(), joshResults.toArray());
		assertArrayEquals(versesFromFile.subList(14236, 14242).toArray(), psalmResults.toArray());
	}
	
	@Test
	public void TestWholeChaptersValid() {
		VerseList songResults = getVersesForReference("Song of Solomon 3");
		VerseList revResults = getVersesForReference("Revelation 22");
		VerseList timResults = getVersesForReference("1 Tim 2-4");
		VerseList johnResults = getVersesForReference("1 John 2-3");
		
		assertArrayEquals(versesFromFile.subList(17572, 17583).toArray(), songResults.toArray());
		assertArrayEquals(versesFromFile.subList(31081, 31102).toArray(), revResults.toArray());
		assertArrayEquals(versesFromFile.subList(29717, 29764).toArray(), timResults.toArray());
		assertArrayEquals(versesFromFile.subList(30551, 30604).toArray(), johnResults.toArray());
	}
	
	@Test
	public void TestFromMultipleChaptersValid() {
		VerseList isaResults = getVersesForReference("Isa 52:13 - 53:12");
		VerseList malResults = getVersesForReference("Mal 3:6-4:6");
		
		assertArrayEquals(versesFromFile.subList(18709, 18724).toArray(), isaResults.toArray());
		assertArrayEquals(versesFromFile.subList(23126, 23145).toArray(), malResults.toArray());
	}
	
	@Test
	public void TestWholeBooksValid() {
		VerseList kingResults = getVersesForReference("1 Kings");
		VerseList philResults = getVersesForReference("Philemon");
		
		assertArrayEquals(versesFromFile.subList(8718, 9534).toArray(), kingResults.toArray());
		assertArrayEquals(versesFromFile.subList(29939, 29964).toArray(), philResults.toArray());
	}
	
	@Test
	public void TestOddSyntaxValid() {
		VerseList ephResults = getVersesForReference("Ephesians 5-6:9");
		VerseList hebResults = getVersesForReference("Hebrews 11-12:2");
		
		assertArrayEquals(versesFromFile.subList(29305, 29347).toArray(), ephResults.toArray());
		assertArrayEquals(versesFromFile.subList(30173, 30215).toArray(), hebResults.toArray());
	}
	
	@Test
	public void TestInvalidReference() {
		VerseList judeResults = getVersesForReference("Jude 2");
		VerseList hermanResults = getVersesForReference("Herman 2");
		VerseList johnResults = getVersesForReference("John 3:163");
		VerseList malResults = getVersesForReference("Mal 13:6-24:7");

		assertArrayEquals(new ArrayList<Verse>().toArray(), judeResults.toArray());
		assertArrayEquals(new ArrayList<Verse>().toArray(), hermanResults.toArray());
		assertArrayEquals(new ArrayList<Verse>().toArray(), johnResults.toArray());
		assertArrayEquals(new ArrayList<Verse>().toArray(), malResults.toArray());
	}
	
	@Test
	public void TestInvalidSyntax() {
		VerseList timResults = getVersesForReference("1Tim 3-2");
		VerseList deutResults = getVersesForReference("Deut :2-3");
		VerseList joshResults = getVersesForReference("Josh 6:4- :6");
		VerseList ruthResults = getVersesForReference("Ruth : - :");
		VerseList samResults = getVersesForReference("2 Sam : 4-7 :");
		VerseList ephResults = getVersesForReference("Ephesians 5:2,4");
		VerseList johnResults = getVersesForReference("John 3;16");
		
		assertArrayEquals(new ArrayList<Verse>().toArray(), timResults.toArray());
		assertArrayEquals(new ArrayList<Verse>().toArray(), deutResults.toArray());
		assertArrayEquals(new ArrayList<Verse>().toArray(), joshResults.toArray());
		assertArrayEquals(new ArrayList<Verse>().toArray(), ruthResults.toArray());
		assertArrayEquals(new ArrayList<Verse>().toArray(), samResults.toArray());
		assertArrayEquals(new ArrayList<Verse>().toArray(), ephResults.toArray());
		assertArrayEquals(new ArrayList<Verse>().toArray(), johnResults.toArray());
	}
}
