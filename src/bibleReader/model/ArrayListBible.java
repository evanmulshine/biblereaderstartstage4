package bibleReader.model;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * A class that stores a version of the Bible.
 * 
 * @author Chuck Cusack (Provided the interface). Modified February 9, 2015.
 * @author Evan Mulshine (provided the implementation)
 * @author Dre Solorzano (provided the implementation)
 */
public class ArrayListBible implements Bible {

	// The Fields
	private ArrayList<Verse> verses;
	private String version;
	private String title;

	/**
	 * Create a new Bible with the given verses.
	 * 
	 * @param verses All of the verses of this version of the Bible.
	 */
	public ArrayListBible(VerseList verses) {
		this.verses = new ArrayList<Verse>(verses.copyVerses());
		this.version = verses.getVersion();
		this.title = verses.getDescription();
	}

	@Override
	public int getNumberOfVerses() {
		return verses.size();
	}

	@Override
	public String getVersion() {
		return version;
	}

	@Override
	public String getTitle() {
		return title;
	}

	@Override
	public boolean isValid(Reference ref) {
		if (ref.getBookOfBible() != null) {
			VerseList list = new VerseList(version, title, verses);
			return list.indexOfVerseWithReference(ref) != -1;
		} else {
			return false;
		}
	}

	@Override
	public String getVerseText(Reference r) {
		for (Verse verse : verses) {

			if (verse.getReference().equals(r)) {
				return verse.getText();
			}
		}
		return null;
	}

	@Override
	public Verse getVerse(Reference r) {
		for (Verse verse : verses) {

			if (verse.getReference().equals(r)) {
				return verse;
			}
		}
		return null;
	}

	@Override
	public Verse getVerse(BookOfBible book, int chapter, int verse) {
		Reference reference = new Reference(book, chapter, verse);
		return getVerse(reference);
	}

	@Override
	public VerseList getAllVerses() {
		return new VerseList(version, title, verses);
	}

	@Override
	public VerseList getVersesContaining(String phrase) {
		ArrayList<Verse> list = new ArrayList<Verse>();
		String search = phrase.trim().toLowerCase();
		
		if (!phrase.equals("")) {
			for (Verse verse : verses) {

				if (verse.getText().trim().toLowerCase().contains(search)) {
					list.add(verse);
				}
			}
		}
		return new VerseList(version, phrase, list);
	}

	@Override
	public ArrayList<Reference> getReferencesContaining(String phrase) {
		ArrayList<Reference> list = new ArrayList<Reference>();
		String search = phrase.trim().toLowerCase();
		
		if (!phrase.equals("")) {

			for (Verse verse : verses) {

				if (verse.getText().trim().toLowerCase().contains(search)) {
					list.add(verse.getReference());
				}
			}
		}
		return list;
	}

	@Override
	public VerseList getVerses(ArrayList<Reference> references) {
		ArrayList<Verse> list = new ArrayList<Verse>();

		for (Reference reference : references) {
			list.add(getVerse(reference));
		}
		return new VerseList(version, "Arbitrary list of Verses", list);
	}

	@Override
	public int getLastVerseNumber(BookOfBible book, int chapter) {
		int verseCount = 0;
		if (isValid(new Reference(book, chapter, 1))) {
			for (Verse verse : verses) {
				Reference ref = verse.getReference();
				BookOfBible refBook = ref.getBookOfBible();
				int refChapter = ref.getChapter();
				if (refBook.equals(book) && refChapter == chapter && ref.getVerse() > verseCount) {
					verseCount = ref.getVerse();
				} else if (verseCount > 0 && (!refBook.equals(book) || refChapter != chapter)) {
					return verseCount;
				}
			}
			return verseCount;
		}		
		return -1;
	}

	@Override
	public int getLastChapterNumber(BookOfBible book) {
		int chapterCount = 0;
		if (isValid(new Reference(book, 1, 1))) {
			for (Verse verse : verses) {
				Reference ref = verse.getReference();
				BookOfBible refBook = ref.getBookOfBible();
				if (refBook.equals(book) && ref.getChapter() > chapterCount) {
					chapterCount = ref.getChapter();
				} else if (chapterCount > 0 && !refBook.equals(book)) {
					return chapterCount;
				}
				
			}
			return chapterCount;
		}			
		return -1;
	}

	@Override
	public ArrayList<Reference> getReferencesInclusive(Reference firstVerse, Reference lastVerse) {
		ArrayList<Reference> refs = new ArrayList<Reference>();
		if (isValid(firstVerse) && isValid(lastVerse) && firstVerse.compareTo(lastVerse) <= 0) {
			for (Verse verse: verses) {
				Reference ref =  verse.getReference(); 
				if (ref.compareTo(firstVerse) >= 0 && ref.compareTo(lastVerse) <=0 ) {
					refs.add(ref);
				}
			}
		}
		return refs;
	}

	@Override
	public ArrayList<Reference> getReferencesExclusive(Reference firstVerse, Reference lastVerse) {
		ArrayList<Reference> refs = new ArrayList<Reference>();
		if (isValid(firstVerse) && firstVerse.compareTo(lastVerse) <= 0) {
			for (Verse verse: verses) {
				Reference ref =  verse.getReference(); 
				if (ref.compareTo(firstVerse) >= 0 && ref.compareTo(lastVerse) < 0 ) {
					refs.add(ref);
				}
			}
		}
		return refs;
	}

	@Override
	public ArrayList<Reference> getReferencesForBook(BookOfBible book) {
		if (book != null) {
			return getReferencesExclusive(new Reference(book, 1, 1), new Reference(BookOfBible.nextBook(book), 1, 1));
		}
		else {
			return new ArrayList<Reference>();
		}
	}

	@Override
	public ArrayList<Reference> getReferencesForChapter(BookOfBible book, int chapter) {
		return getReferencesExclusive(new Reference(book, chapter, 1), new Reference(book, chapter + 1, 1));
	}

	@Override
	public ArrayList<Reference> getReferencesForChapters(BookOfBible book, int chapter1, int chapter2) {
		return getReferencesExclusive(new Reference(book, chapter1, 1), new Reference(book, chapter2 + 1, 1));
	}

	@Override
	public ArrayList<Reference> getReferencesForPassage(BookOfBible book, int chapter, int verse1, int verse2) {
		return getReferencesInclusive(new Reference(book, chapter, verse1), new Reference(book, chapter, verse2));
	}

	@Override
	public ArrayList<Reference> getReferencesForPassage(BookOfBible book, int chapter1, int verse1, int chapter2,
			int verse2) {
		return getReferencesInclusive(new Reference(book, chapter1, verse1), new Reference(book, chapter2, verse2));
	}

	@Override
	public VerseList getVersesInclusive(Reference firstVerse, Reference lastVerse) {
		ArrayList<Verse> list = new ArrayList<Verse>();
		if (isValid(firstVerse) && isValid(lastVerse) && firstVerse.compareTo(lastVerse) <= 0) {
			for (Verse verse: verses) {
				Reference ref =  verse.getReference(); 
				if (ref.compareTo(firstVerse) >= 0 && ref.compareTo(lastVerse) <=0 ) {
					list.add(verse);
				}
			}
		}
		return new VerseList(version, "Verses from " + firstVerse + " to " + lastVerse, list);
	}

	@Override
	public VerseList getVersesExclusive(Reference firstVerse, Reference lastVerse) {
		ArrayList<Verse> list = new ArrayList<Verse>();
		if (isValid(firstVerse) && firstVerse.compareTo(lastVerse) <= 0) {
			for (Verse verse: verses) {
				Reference ref =  verse.getReference(); 
				if (ref.compareTo(firstVerse) >= 0 && ref.compareTo(lastVerse) < 0 ) {
					list.add(verse);
				}
			}
		}
		return new VerseList(version, "Verses from " + firstVerse + " to " + lastVerse + " excluding the final one", list);
	}

	@Override
	public VerseList getBook(BookOfBible book) {
		ArrayList<Verse> list = new ArrayList<Verse>();
		for (Verse verse : verses) {
			Reference ref =  verse.getReference(); 
			if (ref.getBookOfBible().equals(book)) {
				list.add(verse);
			}
		}
		return new VerseList(version, "<i>" + book + "</i>", list);
	}

	@Override
	public VerseList getChapter(BookOfBible book, int chapter) {
		ArrayList<Verse> list = new ArrayList<Verse>();
		for (Verse verse : verses) {
			Reference ref = verse.getReference();
			if (ref.getBookOfBible().equals(book) && ref.getChapter() == chapter) {
				list.add(verse);
			}
		}
		return new VerseList(version, "" + book + chapter, list);
	}

	@Override
	public VerseList getChapters(BookOfBible book, int chapter1, int chapter2) {
		ArrayList<Verse> list = new ArrayList<Verse>();
		Reference firstVerse = new Reference(book, chapter1, 1);
		Reference lastVerse = new Reference(book, chapter2 + 1, 1);
		if (isValid(firstVerse) && firstVerse.compareTo(lastVerse) <= 0) {
			for (Verse verse: verses) {
				Reference ref =  verse.getReference(); 
				if (ref.compareTo(firstVerse) >= 0 && ref.compareTo(lastVerse) < 0 ) {
					list.add(verse);
				}
			}
		}
		return new VerseList(version, "" + book + chapter1+"-"+chapter2, list);
	}

	@Override
	public VerseList getPassage(BookOfBible book, int chapter, int verse1, int verse2) {
		ArrayList<Verse> list = new ArrayList<Verse>();
		Reference firstVerse = new Reference(book, chapter, verse1);
		Reference lastVerse = new Reference(book, chapter, verse2);
		if (isValid(firstVerse) && isValid(lastVerse) && firstVerse.compareTo(lastVerse) <= 0) {
			for (Verse verse: verses) {
				Reference ref =  verse.getReference(); 
				if (ref.compareTo(firstVerse) >= 0 && ref.compareTo(lastVerse) <= 0 ) {
					list.add(verse);
				}
			}
		}
		return new VerseList(version, "" + book + chapter + ":" + verse1 + "-" + verse2, list);
	}

	@Override
	public VerseList getPassage(BookOfBible book, int chapter1, int verse1, int chapter2, int verse2) {
		ArrayList<Verse> list = new ArrayList<Verse>();
		Reference firstVerse = new Reference(book, chapter1, verse1);
		Reference lastVerse = new Reference(book, chapter2, verse2);
		if (isValid(firstVerse) && isValid(lastVerse) && firstVerse.compareTo(lastVerse) <= 0) {
			for (Verse verse: verses) {
				Reference ref =  verse.getReference(); 
				if (ref.compareTo(firstVerse) >= 0 && ref.compareTo(lastVerse) <= 0 ) {
					list.add(verse);
				}
			}
		}
		return new VerseList(version, "" + book + chapter1 + ":" + verse1 + "-" + chapter2 + ":" + verse2, list);
	}
}
