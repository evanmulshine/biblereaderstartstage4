package bibleReader.model;

import java.util.ArrayList;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;

/**
 * A class that stores a version of the Bible.
 * 
 * @author Chuck Cusack (Provided the interface)
 * @author Evan Mulshine (provided the implementation)
 */
public class TreeMapBible implements Bible {

	// The Fields
	private String version;
	private String title;
	private TreeMap<Reference, Verse> theVerses;

	/**
	 * Create a new Bible with the given verses.
	 * 
	 * @param verses All of the verses of this version of the Bible.
	 */
	public TreeMapBible(VerseList verses) {
		theVerses = new TreeMap<Reference, Verse>();
		
		for (Verse verse : verses) {
			theVerses.put(verse.getReference(), verse);
		}
		version = verses.getVersion();
		title = verses.getDescription();
	}

	@Override
	public int getNumberOfVerses() {
		return theVerses.size();
	}

	@Override
	public VerseList getAllVerses() {
		VerseList verses = new VerseList(version, title);
		Set<Map.Entry<Reference, Verse>> mySet = theVerses.entrySet();
		
		for (Map.Entry<Reference, Verse> element : mySet) {
			verses.add(element.getValue());
		}
		return verses;
	}

	@Override
	public String getVersion() {
		return version;
	}

	@Override
	public String getTitle() {
		return title;
	}

	@Override
	public boolean isValid(Reference ref) {
		if (ref.getBookOfBible() == null) {
			return false;
		}
		return theVerses.containsKey(ref);
	}

	@Override
	public String getVerseText(Reference r) {
		Verse verse = theVerses.get(r);
		if (verse != null) {
			return verse.getText();
		}
		else {
			return null;
		}
	}

	@Override
	public Verse getVerse(Reference r) {
		return theVerses.get(r);
	}

	@Override
	public Verse getVerse(BookOfBible book, int chapter, int verse) {
		Reference r = new Reference(book, chapter, verse);
		return theVerses.get(r);
	}

	@Override
	public VerseList getVersesContaining(String phrase) {
		VerseList verses = new VerseList(version, phrase);
		String search = phrase.trim().toLowerCase();
		
		if (!phrase.equals("")) {
			for (Verse verse : getAllVerses()) {

				if (verse.getText().trim().toLowerCase().contains(search)) {
					verses.add(verse);
				}
			}
		}
		return verses;
	}

	@Override
	public ArrayList<Reference> getReferencesContaining(String phrase) {
		ArrayList<Reference> list = new ArrayList<Reference>();
		String search = phrase.trim().toLowerCase();
		
		if (!phrase.equals("")) {
			Set<Map.Entry<Reference, Verse>> mySet = theVerses.entrySet();
			
			for (Map.Entry<Reference, Verse> element : mySet) {
				
				if (element.getValue().getText().trim().toLowerCase().contains(search)) {
					list.add(element.getKey());
				}
			}
		}
		return list;
	}

	@Override
	public VerseList getVerses(ArrayList<Reference> references) {
		VerseList verses = new VerseList(version, "Arbitrary list of Verses");
		
		for (Reference ref : references) {
			if (ref != null && ref.getBookOfBible() != null) {
				verses.add(theVerses.get(ref));
			}
			else {
				verses.add(null);
			}
		}
		return verses;
	}


	@Override
	public int getLastVerseNumber(BookOfBible book, int chapter) {
		int verseCount = -1;
		Reference ref = new Reference(book, chapter + 1, 1);
		verseCount = theVerses.lowerKey(ref).getVerse();
		return verseCount;
	}

	@Override
	public int getLastChapterNumber(BookOfBible book) {
		int chapterCount = -1;
		
		if (book != null) {
			Reference ref = new Reference(BookOfBible.nextBook(book), 1, 1);
			chapterCount = theVerses.lowerKey(ref).getChapter();
		}			
		return chapterCount;
	}

	@Override
	public ArrayList<Reference> getReferencesInclusive(Reference firstVerse, Reference lastVerse) {
		Reference last = new Reference(lastVerse.getBookOfBible(), lastVerse.getChapter(), lastVerse.getVerse() + 1);
		return getReferencesExclusive(firstVerse, last);
	}

	@Override
	public ArrayList<Reference> getReferencesExclusive(Reference firstVerse, Reference lastVerse) {
		ArrayList<Reference> refs = new ArrayList<Reference>();
		
		if (isValid(firstVerse) && firstVerse.compareTo(lastVerse) <= 0) {
			
			SortedMap<Reference, Verse> s = theVerses.subMap(firstVerse, lastVerse);
			Set<Map.Entry<Reference, Verse>> mySet = s.entrySet();
			
			for (Map.Entry<Reference, Verse> element : mySet) {
				refs.add(element.getKey());
			}
		}
		return refs;
	}

	@Override
	public ArrayList<Reference> getReferencesForBook(BookOfBible book) {
		if (book != null) {
			return getReferencesExclusive(new Reference(book, 1, 1), new Reference(BookOfBible.nextBook(book), 1, 1));
		}
		else {
			return new ArrayList<Reference>();
		}
	}

	@Override
	public ArrayList<Reference> getReferencesForChapter(BookOfBible book, int chapter) {
		return getReferencesExclusive(new Reference(book, chapter, 1), new Reference(book, chapter + 1, 1));
	}

	@Override
	public ArrayList<Reference> getReferencesForChapters(BookOfBible book, int chapter1, int chapter2) {
		return getReferencesExclusive(new Reference(book, chapter1, 1), new Reference(book, chapter2 + 1, 1));
	}

	@Override
	public ArrayList<Reference> getReferencesForPassage(BookOfBible book, int chapter, int verse1, int verse2) {
		return getReferencesInclusive(new Reference(book, chapter, verse1), new Reference(book, chapter, verse2));
	}

	@Override
	public ArrayList<Reference> getReferencesForPassage(BookOfBible book, int chapter1, int verse1, int chapter2, int verse2) {
		return getReferencesInclusive(new Reference(book, chapter1, verse1), new Reference(book, chapter2, verse2));
	}

	@Override
	public VerseList getVersesInclusive(Reference firstVerse, Reference lastVerse) {
		// We will store the resulting verses here. We copy the version from
		// this Bible and set the description to be the passage that was searched for.
		VerseList someVerses = new VerseList(version, firstVerse + "-" + lastVerse);
		
		// Make sure the references are valid and in the correct order. If not, return an empty list.
		if (!isValid(firstVerse) || !isValid(lastVerse) || firstVerse.compareTo(lastVerse) > 0) {
			return someVerses;
		}
		
		// Add 1 to the verse number of the last verse because subMap excludes the last key.
		Reference lastRef = new Reference(lastVerse.getBookOfBible(), lastVerse.getChapter(), lastVerse.getVerse() + 1);
		
		// Return the portion of the TreeMap that contains the verses between
		// the first and the last, not including the last.
		SortedMap<Reference, Verse> s = theVerses.subMap(firstVerse, lastRef);
		
		// Get the entries from the map so we can iterate through them.
		Set<Map.Entry<Reference, Verse>> mySet = s.entrySet();
		
		// Iterate through the set and put the verses in the VerseList.
		for (Map.Entry<Reference, Verse> element : mySet) {
			someVerses.add(element.getValue());
		}
		return someVerses;
	}

	@Override
	public VerseList getVersesExclusive(Reference firstVerse, Reference lastVerse) {
		// Implementation of this method provided by Chuck Cusack.
		// This is provided so you have an example to help you get started
		// with the other methods.

		// We will store the resulting verses here. We copy the version from
		// this Bible and set the description to be the passage that was searched for.
		VerseList someVerses = new VerseList(getVersion(), firstVerse + "-" + lastVerse + " excluding the final one");

		// Make sure the references are in the correct order. If not, return an empty list.
		if (firstVerse.compareTo(lastVerse) > 0) {
			return someVerses;
		}
		// Return the portion of the TreeMap that contains the verses between
		// the first and the last, not including the last.
		SortedMap<Reference, Verse> s = theVerses.subMap(firstVerse, lastVerse);

		// Get the entries from the map so we can iterate through them.
		Set<Map.Entry<Reference, Verse>> mySet = s.entrySet();

		// Iterate through the set and put the verses in the VerseList.
		for (Map.Entry<Reference, Verse> element : mySet) {
			someVerses.add(element.getValue());
		}
		return someVerses;
	}

	@Override
	public VerseList getBook(BookOfBible book) {
		if (book == null) {
			return new VerseList(version, "");
		}
		VerseList someVerses = new VerseList(version, book.toString());
		Reference firstVerse = new Reference(book, 1, 1);
		Reference lastVerse = new Reference(BookOfBible.nextBook(book), 1, 1);
		
		// Return the portion of the TreeMap that contains the verses between
		// the first and the last, not including the last.
		SortedMap<Reference, Verse> s = theVerses.subMap(firstVerse, lastVerse);

		// Get the entries from the map so we can iterate through them.
		Set<Map.Entry<Reference, Verse>> mySet = s.entrySet();

		// Iterate through the set and put the verses in the VerseList.
		for (Map.Entry<Reference, Verse> element : mySet) {
			someVerses.add(element.getValue());
		}
		return someVerses;
	}

	@Override
	public VerseList getChapter(BookOfBible book, int chapter) {
		if (book == null) {
			return new VerseList(version, "");
		}
		VerseList someVerses = new VerseList(version, book.toString() + chapter);
		Reference firstVerse = new Reference(book, chapter, 1);
		Reference lastVerse = new Reference(book, chapter + 1, 1);
		
		// Return the portion of the TreeMap that contains the verses between
		// the first and the last, not including the last.
		SortedMap<Reference, Verse> s = theVerses.subMap(firstVerse, lastVerse);

		// Get the entries from the map so we can iterate through them.
		Set<Map.Entry<Reference, Verse>> mySet = s.entrySet();

		// Iterate through the set and put the verses in the VerseList.
		for (Map.Entry<Reference, Verse> element : mySet) {
			someVerses.add(element.getValue());
		}
		return someVerses;
	}

	@Override
	public VerseList getChapters(BookOfBible book, int chapter1, int chapter2) {
		if (book == null || chapter1 > chapter2) {
			return new VerseList(version, "");
		}
		VerseList someVerses = new VerseList(version, book.toString() + chapter1 + "-" + chapter2);
		Reference firstVerse = new Reference(book, chapter1, 1);
		Reference lastVerse = new Reference(book, chapter2 + 1, 1);
		
		// Return the portion of the TreeMap that contains the verses between
		// the first and the last, not including the last.
		SortedMap<Reference, Verse> s = theVerses.subMap(firstVerse, lastVerse);

		// Get the entries from the map so we can iterate through them.
		Set<Map.Entry<Reference, Verse>> mySet = s.entrySet();

		// Iterate through the set and put the verses in the VerseList.
		for (Map.Entry<Reference, Verse> element : mySet) {
			someVerses.add(element.getValue());
		}
		return someVerses;
	}

	@Override
	public VerseList getPassage(BookOfBible book, int chapter, int verse1, int verse2) {
		if (book == null || verse1 > verse2) {
			return new VerseList(version, "");
		}
		VerseList someVerses = new VerseList(version, book.toString() + chapter + ":" + verse1 + "-" + verse2);
		Reference firstVerse = new Reference(book, chapter, verse1);
		Reference lastVerse = new Reference(book, chapter, verse2 + 1);
		
		// Return the portion of the TreeMap that contains the verses between
		// the first and the last, not including the last.
		SortedMap<Reference, Verse> s = theVerses.subMap(firstVerse, lastVerse);

		// Get the entries from the map so we can iterate through them.
		Set<Map.Entry<Reference, Verse>> mySet = s.entrySet();

		// Iterate through the set and put the verses in the VerseList.
		for (Map.Entry<Reference, Verse> element : mySet) {
			someVerses.add(element.getValue());
		}
		return someVerses;
	}

	@Override
	public VerseList getPassage(BookOfBible book, int chapter1, int verse1, int chapter2, int verse2) {
		if (book == null || chapter1 > chapter2) {
			return new VerseList(version, "");
		}
		VerseList someVerses = new VerseList(version, book.toString() + chapter1 + ":" + verse1 + "-" + chapter2 + ":" + verse2);
		Reference firstVerse = new Reference(book, chapter1, verse1);
		Reference lastVerse = new Reference(book, chapter2, verse2 + 1);
		
		// Return the portion of the TreeMap that contains the verses between
		// the first and the last, not including the last.
		SortedMap<Reference, Verse> s = theVerses.subMap(firstVerse, lastVerse);

		// Get the entries from the map so we can iterate through them.
		Set<Map.Entry<Reference, Verse>> mySet = s.entrySet();

		// Iterate through the set and put the verses in the VerseList.
		for (Map.Entry<Reference, Verse> element : mySet) {
			someVerses.add(element.getValue());
		}
		return someVerses;
	}

}
