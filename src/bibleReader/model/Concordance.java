package bibleReader.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.TreeSet;

/**
 * Concordance is a class which implements a concordance for a Bible. In other words, it allows the easy lookup of all
 * references which contain a given word.
 * 
 * @author Chuck Cusack, March 2013 (Provided the interface)
 * @author Evan Mulshine, April 2019 (Provided the implementation details)
 * @author Dre Solorzano, April 2019 (Provided the implementation details)
 */
public class Concordance {
	private HashMap<String, TreeSet<Reference>> map;
	
	/**
	 * Construct a concordance for the given Bible.
	 */
	public Concordance(Bible bible) {
		map = new HashMap<String, TreeSet<Reference>>();
		
		for (Verse verse : bible.getAllVerses()) {
			Reference ref = verse.getReference();
			ArrayList<String> words = extractWords(verse.getText());
			
			for (String word : words) {

				// If there is already an entry in the concordance for this word, add the reference to its list.
				if (map.containsKey(word)) {
					map.get(word).add(ref);
				} else {
					// If not, make a new entry for it and add the reference to its list.
					TreeSet<Reference> set = new TreeSet<Reference>();
					set.add(ref);
					map.put(word, set);
				}
			}
		}
	}

	/**
	 * Return the list of references to verses that contain the word 'word' (ignoring case) in the version of the Bible
	 * that this concordance was created with.
	 * 
	 * @param word a single word (no spaces, etc.)
	 * @return the list of References of verses from this version that contain the word, or an empty list if no verses
	 *         contain the word.
	 */
	public ArrayList<Reference> getReferencesContaining(String word) {
		String search = word.toLowerCase();
		TreeSet<Reference> refSet = map.get(search);
		
		if (refSet != null) {
			return new ArrayList<Reference>(refSet);
		}
		else {
			return new ArrayList<Reference>();
		}
	}

	/**
	 * Given an array of Strings, where each element of the array is expected to be a single word (with no spaces, etc.,
	 * but ignoring case), return a ArrayList<Reference> containing all of the verses that contain <i>all of the words</i>.
	 * 
	 * @param words A list of words.
	 * @return An ArrayList<Reference> containing references to all of the verses that contain all of the given words, or an
	 *         empty list if
	 */
	public ArrayList<Reference> getReferencesContainingAll(ArrayList<String> words) {
		ArrayList<Reference> list = new ArrayList<Reference>();
		
		if (words.size() == 0) {
			return list;
		}
		// Find all the references containing the first word
		String first = words.get(0).toLowerCase();
		TreeSet<Reference> firstSet = map.get(first);
		
		if (firstSet == null) {
			return list;
		}
		list.addAll(firstSet);
		
		// Iterate through the rest of the words and get rid of any previously found results if they don't contain that word.
		for (int i = 1; i < words.size(); i++) {	
			String word = words.get(i).toLowerCase();
			TreeSet<Reference> wordSet = map.get(word);
			if (wordSet == null) {
				return new ArrayList<Reference>();
			}
			list.retainAll(wordSet);
		}
		return list;
	}
	
	public static ArrayList<String> extractWords (String text) {
		text = text.toLowerCase();
		// removes a few HTML tags (relevant to ESV) and 's at end of words.
		// Replaces them with space so words around them don't get squashed
		// together. Notice the two types of apostrophe-each is used in a 
		// different version.
		text = text.replaceAll("(<sup>[,\\w]*?</sup>|'s|�s|&#\\w*;|,)", " ");
		//remove commas. This should help us match numbers better.
		text = text.replaceAll(",",  "");
		String [] words = text.split("\\W+");
		HashSet<String> toRet = new HashSet<String>(Arrays.asList(words));
		toRet.remove("");
		return new ArrayList<String>(toRet);
	}
	
}
