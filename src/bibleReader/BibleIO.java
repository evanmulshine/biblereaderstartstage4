package bibleReader;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

import bibleReader.model.Bible;
import bibleReader.model.BookOfBible;
import bibleReader.model.Reference;
import bibleReader.model.Verse;
import bibleReader.model.VerseList;

/**
 * A utility class that has useful methods to read/write Bibles and Verses.
 * 
 * @author cusack
 * @author Evan Mulshine
 */
public class BibleIO {

	/**
	 * Read in a file and create a Bible object from it and return it.
	 * 
	 * @param bibleFile
	 * @return
	 */
	public static VerseList readBible(File bibleFile) { // Get the extension of
														// the file
		String name = bibleFile.getName();
		String extension = name.substring(name.lastIndexOf('.') + 1, name.length());

		// Call the read method based on the file type.
		if ("atv".equals(extension.toLowerCase())) {
			return readATV(bibleFile);
		} else if ("xmv".equals(extension.toLowerCase())) {
			return readXMV(bibleFile);
		} else {
			return null;
		}
	}

	/**
	 * Read in a Bible that is saved in the "ATV" format. The format is described
	 * below.
	 * 
	 * @param bibleFile The file containing a Bible with .atv extension.
	 * @return A Bible object constructed from the file bibleFile, or null if there
	 *         was an error reading the file.
	 */
	private static VerseList readATV(File bibleFile) {
		try {
			// Initialize local variables.
			BufferedReader br = new BufferedReader(new FileReader(bibleFile));
			String version;
			String description;
			String firstLine = br.readLine();
			
			// Set version and description.
			if (!firstLine.equals("")) {
				String[] info = firstLine.split(":");
				
				if (info.length > 1) {
					version = info[0].trim();
					description = info[1].trim();
				} else {
					version = info[0].trim();
					description = "";
				}
			} else {
				version = "unknown";
				description = "";
			}
			
			// Set verses.
			VerseList verses = new VerseList(version, description);
			
			while (br.ready()) {
				// Split the line into parts.
				String line = br.readLine();
				String[] parts = line.split("@");
				String[] chapverse = parts[1].split(":");
				
				// Set the verse information.
				BookOfBible book = BookOfBible.getBookOfBible(parts[0]);
				int chapter = Integer.parseInt(chapverse[0]);
				int versenum = Integer.parseInt(chapverse[1]);
				String text = parts[2];
				
				if (book != null) {
					// Add the verse to the list.
					verses.add(new Verse(book, chapter, versenum, text));
				} else {
					// Close reader and return null if book abbreviation is invalid.
					br.close();
					return null;
				}
			}
			// Close the reader and return the verse list, if everything went smoothly.
			br.close();
			return verses;
		} catch (IOException e) {
			e.printStackTrace();
		} catch (RuntimeException e) {
			// Return null if there are any problems parsing verses.
			return null;
		}
		return null;
	}

	/**
	 * Read in the Bible that is stored in the XMV format.
	 * The first line should be in the format "<Version [version]: [description]>"
	 * At the beginning of each book is a line in the format "<Book [book], description of book>"
	 * At the beginning of each chapter is a line in the format "<Chapter [chapter]>"
	 * Each verse is in the format "<Verse [verse]>[text]"
	 * 
	 * @param bibleFile The file containing a Bible with .xmv extension.
	 * @return A Bible object constructed from the file bibleFile, or null if there
	 *         was an error reading the file.
	 */
	private static VerseList readXMV(File bibleFile) {

		try {
			// Set local variables.
			BufferedReader br = new BufferedReader(new FileReader(bibleFile));
			String version = "unknown";
			String description = "";
			String firstLine = br.readLine();
			BookOfBible book = null;
			int chapter = 0;
			
			// Set version and description.
			if (firstLine.startsWith("<Version")) {
				String[] parts = firstLine.split(":", 2);
				
				if (parts[0].length() >= 8) {
					version = parts[0].substring(8).trim();
				}
				if (parts.length >= 2) {
					description = parts[1].trim();
				}
			}
			
			// Set verses.
			VerseList verses = new VerseList(version, description);
			
			while (br.ready()) {
				String line = br.readLine();
				
				if (line.startsWith("<Book")) {
					// Set the book.
					String[] parts = line.split(",");
					book = BookOfBible.getBookOfBible(parts[0].substring(5).trim());
					
					if (book == null) {
						// Close the reader and return null if the book is invalid.
						br.close();
						return null;
					}
				} else if (line.startsWith("<Chapter")) {
					// Set the chapter.
					String[] parts = line.split(">");
					chapter = Integer.parseInt(parts[0].substring(9).trim());
				} else if (line.startsWith("<Verse")) {
					// Add the verse to the list.
					String[] parts = line.split(">");
					int verse = Integer.parseInt(parts[0].substring(7).trim());
					String text = parts[1].trim();
					verses.add(new Verse(book, chapter, verse, text));
				}
			}
			
			// Close the reader and return the VerseList.
			br.close();
			return verses;
		} catch (IOException e) {
			e.printStackTrace();
		} catch (RuntimeException e) {
			// Return null if there are any problems parsing the file.
			return null;
		}
		return null;
	}

	/**
	 * Write out the Bible in the ATV format.
	 * 
	 * @param file  The file that the Bible should be written to.
	 * @param bible The Bible that will be written to the file.
	 */
	public static void writeBibleATV(File file, Bible bible) {
		String version = bible.getVersion();
		String desc = bible.getTitle();
		String firstLine = version + ": " + desc;
		writeVersesATV(file, firstLine, bible.getAllVerses());
	}

	/**
	 * Write out the given verses in the ATV format, using the description as the
	 * first line of the file.
	 * 
	 * @param file        The file that the Bible should be written to.
	 * @param description The contents that will be placed on the first line of the
	 *                    file, formatted appropriately.
	 * @param verses      The verses that will be written to the file.
	 */
	public static void writeVersesATV(File file, String description, VerseList verses) {
		try {
			PrintWriter writer = new PrintWriter(new FileWriter(file));
			writer.println(description);
			for (Verse verse : verses) {
				Reference ref = verse.getReference();
				String book = ref.getBookOfBible().toString();
				int chapter = ref.getChapter();
				int verseNum = ref.getVerse();
				String text = verse.getText();
				writer.println(book + "@" + chapter + ":" + verseNum + "@" + text);
			}
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Write the string out to the given file. It is presumed that the string is an
	 * HTML rendering of some verses, but really it can be anything.
	 * 
	 * @param file
	 * @param text
	 */
	public static void writeText(File file, String text) {
		try {
			PrintWriter writer = new PrintWriter(new FileWriter(file));
			writer.write(text);
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
